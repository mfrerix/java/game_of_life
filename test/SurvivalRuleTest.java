import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SurvivalRuleTest {

    @Test
    public void lessThenTwoNeighborsWillKillCell() {
        Rule rule = new SurvivalRule();
        Field field = new DefaultBorderedField();
        field.setLife(5,5);
        field.setLife(6,5);
        assertFalse(rule.isFieldAliveNextGeneration(5,5,field));
        assertFalse(rule.isFieldAliveNextGeneration(6,5,field));
    }

    @Test
    public void moreThenTreeNeighborsWillKillCell() {
        Rule rule = new SurvivalRule();
        Field field = new DefaultBorderedField();
        field.setLife(4,4);
        field.setLife(4,5);
        field.setLife(5,5);
        field.setLife(6,5);

        assertFalse(rule.isFieldAliveNextGeneration(5,4, field));
    }

    @Test
    public void twoAndThreeNeighborsWillNotKillCell() {
        Rule rule = new SurvivalRule();
        Field field = new DefaultBorderedField();
        field.setLife(4,4);
        field.setLife(4,5);
        field.setLife(5,5);

        assertTrue(rule.isFieldAliveNextGeneration(5,4, field));

        field.kill(4,4);

        assertTrue(rule.isFieldAliveNextGeneration(5,4, field));
    }
}
