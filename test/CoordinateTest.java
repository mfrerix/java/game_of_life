import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CoordinateTest {


    @Test
    public void hashCodeIsEqualsToStringRepresentationHashCode() {
        Coordinate coordinate = new Coordinate(1,2);
        assertEquals("1/2".hashCode(), ( coordinate).hashCode());
    }

    @Test
    public void stringInFormatXSeparatedSlashY() {
        Coordinate coordinate = new Coordinate(1,2);
        assertEquals("1/2", coordinate.toString());
    }

    @Test
    public void coordinatesEqualsIfXAndYEqual() {
        Coordinate coordinate = new Coordinate(1,2);
        Coordinate equalCoordinate = new Coordinate(1,2);
        Coordinate unequalCoordinate = new Coordinate(1,3);

        assertTrue(coordinate.equals(equalCoordinate));
        assertFalse(coordinate.equals(unequalCoordinate));
        assertFalse(coordinate.equals(1));
    }



}
