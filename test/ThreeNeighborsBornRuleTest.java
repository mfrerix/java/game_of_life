import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ThreeNeighborsBornRuleTest {

    @Test
    public void newLifeWillBornWithThreeNeighbors() {
          Rule rule = new ThreeNeighborsBornRule();
          Field field = new DefaultBorderedField();
          field.setLife(4,5);
          field.setLife(5,5);
          field.setLife(6,5);

          assertTrue(rule.isFieldAliveNextGeneration(5,4, field));
          assertTrue(rule.isFieldAliveNextGeneration(5,6, field));
      }

      @Test
      public void noLifeWillBornByLessThenThreeNeighbors() {
          Rule rule = new ThreeNeighborsBornRule();
          Field field = new DefaultBorderedField();
          field.setLife(4,5);

          field.setLife(6,5);

          assertFalse(rule.isFieldAliveNextGeneration(5,4, field));
          assertFalse(rule.isFieldAliveNextGeneration(5,6, field));
      }

    @Test
    public void noLifeWillBornByMoreThenThreeNeighbors() {
        Rule rule = new ThreeNeighborsBornRule();
        Field field = new DefaultBorderedField();
        field.setLife(4,4);
        field.setLife(4,5);
        field.setLife(5,5);
        field.setLife(6,5);

        assertFalse(rule.isFieldAliveNextGeneration(5,4, field));
    }

}
