import static org.junit.Assert.*;
import org.junit.Test;

public class DefaultRulesetTest {

    @Test
    public void applyRuleToFieldWillCreateNewField() {
        Field field = new DefaultBorderedField();
        Ruleset ruleset = new DefaultRuleset();

        assertNotSame(field, ruleset.applyRulesToField(field));
    }

    @Test
    public void nextGenWillHaveSameDimensionAlsCurrentGen() {
        Field currentGen = new DefaultBorderedField(3,4);
        Ruleset ruleset = new DefaultRuleset();

        Field nextGen = ruleset.applyRulesToField(currentGen);

        assertEquals(3,nextGen.getWidth());
        assertEquals(4,nextGen.getHeight());
    }

    @Test
    public void newLifeWillBornWithThreeNeighbors() {

        Field field = new DefaultBorderedField();
        field.setLife(4,5);
        field.setLife(5,5);
        field.setLife(6,5);

        Ruleset ruleset = new DefaultRuleset();

        Field nextGen = ruleset.applyRulesToField(field);
        assertTrue(nextGen.isAlive(5,4));
        assertTrue(nextGen.isAlive(5,6));
    }
}
