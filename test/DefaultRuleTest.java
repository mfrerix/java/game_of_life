import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultRuleTest {

    @Test
    public void threeNeigborsWillCreateLife() {
        Ruleset rule = new DefaultRuleset();
        Field field = new DefaultBorderedField();
        field.setLife(4,5);
        field.setLife(5,5);
        field.setLife(6,5);
        field = rule.applyRulesToField(field);
        assertTrue(field.isAlive(5,4));
        assertTrue(field.isAlive(5,6));

    }
}
