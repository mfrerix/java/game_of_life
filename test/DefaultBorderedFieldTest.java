import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DefaultBorderedFieldTest {

    @Test
    public void newCreatedFieldDontContainsLive() {
        Field field = new DefaultBorderedField();
        for(int x = 0; x < field.getWidth(); x++) {
            for(int y = 0; y < field.getHeight(); y++) {
                assertFalse(field.isAlive(x,y));
            }
        }
    }

    @Test
    public void defaultFieldHashDefaultDimensions() {
        Field field = new DefaultBorderedField();
        assertEquals(DefaultBorderedField.DEFAULT_WIDTH, field.getWidth());
        assertEquals(DefaultBorderedField.DEFAULT_HEIGHT, field.getHeight());
    }

    @Test
    public void specifigFieldHasSpecifigDimensions() {
        Field field = new DefaultBorderedField(11,11);
        assertEquals(11, field.getWidth());
        assertEquals(11, field.getHeight());
    }

    @Test
    public void lifeCanBeSetAtCoordinate() {
        Field field = new DefaultBorderedField();
        field.setLife(5,5);
        assertTrue(field.isAlive(5,5));
    }

    @Test
    public void lifeCantSetOutOfRange() {
        Field field = new DefaultBorderedField();
        field.setLife(-1,0);
        field.setLife(0,-1);
        field.setLife(-1,-1);
        field.setLife(DefaultBorderedField.DEFAULT_WIDTH+1, DefaultBorderedField.DEFAULT_WIDTH);
        field.setLife(DefaultBorderedField.DEFAULT_WIDTH, DefaultBorderedField.DEFAULT_WIDTH+1);
        field.setLife(DefaultBorderedField.DEFAULT_WIDTH+1, DefaultBorderedField.DEFAULT_WIDTH+1);
        assertFalse(field.isAlive(-1,0));
        assertFalse(field.isAlive(0,-1));
        assertFalse(field.isAlive(-1,-1));
        assertFalse(field.isAlive(DefaultBorderedField.DEFAULT_WIDTH+1, DefaultBorderedField.DEFAULT_WIDTH));
        assertFalse(field.isAlive(DefaultBorderedField.DEFAULT_WIDTH, DefaultBorderedField.DEFAULT_WIDTH+1));
        assertFalse(field.isAlive(DefaultBorderedField.DEFAULT_WIDTH+1, DefaultBorderedField.DEFAULT_WIDTH+1));
    }

    @Test
    public void lifeCanBeKilledAtCoordinate() {
        Field field = new DefaultBorderedField();
        field.setLife(5,5);
        field.kill(5,5);
        assertFalse(field.isAlive(5,5));
    }

    @Test
    public void calculateNeighborLifeCorrect() {
        Field field = new DefaultBorderedField();
        field.setLife(4,5);
        field.setLife(6,6);
        field.setLife(6,4);

        assertEquals(3,field.getCountOfLivingNeighbors(5,5));
    }

    @Test
    public void coordinateIsNotInField() {
        DefaultBorderedField field = new DefaultBorderedField();
        assertFalse(field.isInRange(-1,-1));
        assertFalse(field.isInRange(DefaultBorderedField.DEFAULT_WIDTH+1, DefaultBorderedField.DEFAULT_WIDTH+1));
    }
}
