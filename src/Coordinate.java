public class Coordinate {


    private int x;
    private int y;

    public Coordinate(int x, int y) {

        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return x + "/" + y;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Coordinate)) {
            return false;
        }
        Coordinate other = (Coordinate)obj;
        return x == other.x && y == other.y;
    }
}
