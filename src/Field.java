public interface Field {

    int getHeight();

    int getWidth();

    boolean isAlive(int x, int y);

    void setLife(int x, int y);

    void kill(int x, int y);

    int getCountOfLivingNeighbors(int x, int y);
}
