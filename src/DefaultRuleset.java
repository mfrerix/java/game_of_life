import java.util.ArrayList;
import java.util.List;

public class DefaultRuleset implements Ruleset{

    private Rule bornRule = new ThreeNeighborsBornRule();

    private Rule survivalRule = new SurvivalRule();

    @Override
    public Field applyRulesToField(Field field) {


        Field nextGen = new DefaultBorderedField(field.getWidth(), field.getHeight());

        for(int x = 0; x < field.getWidth(); x++) {
            for(int y = 0; y < field.getHeight(); y++) {
                if(isCellAliveNextGen(x,y,field)) {
                    nextGen.setLife(x,y);
                }
            }
        }

        return nextGen;
    }


    private boolean isCellAliveNextGen(int x, int y, Field field) {
        boolean isAlive = bornRule.isFieldAliveNextGeneration(x, y, field);

        return isAlive;
    }
}
