import java.util.HashSet;

public class DefaultBorderedField implements Field {

    public static final int DEFAULT_WIDTH = 10;
    public static final int DEFAULT_HEIGHT = 10;

    HashSet<Coordinate> livingFields = new HashSet<>();
    private int width;
    private int height;

    public DefaultBorderedField() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DefaultBorderedField(int width, int height) {

        this.width = width;
        this.height = height;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public boolean isAlive(int x, int y) {
        return livingFields.contains(getCoordinateFor(x,y));
    }

    private Coordinate getCoordinateFor(int x,int y) {
        return new Coordinate(x,y);
    }

    @Override
    public void setLife(int x, int y) {
        if(isInRange(x,y)) {
            livingFields.add(getCoordinateFor(x, y));
        }
    }

    @Override
    public void kill(int x, int y) {
        livingFields.remove(getCoordinateFor(x,y));
    }

    @Override
    public int getCountOfLivingNeighbors(int x, int y) {
        int count = 0;
        for(int posX = x-1;posX <=x+1; posX++ ) {
            for(int posY = y-1;posY <=y+1; posY++ ) {
                if(!(posX == x && posY == y)) {
                    if(isAlive(posX,posY)) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    boolean isInRange(int x, int y) {
        boolean xInRange = x >= 0 && x<= getWidth();
        boolean yInRange = y >= 0 && y<= getHeight();
        return xInRange && yInRange;
    }
}
