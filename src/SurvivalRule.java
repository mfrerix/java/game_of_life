public class SurvivalRule implements Rule {

    private static final int LOWER_BORDER_OF_DIE = 1;
    private static final int UPPER_BORDER_OF_DIE = 4;

    @Override
    public boolean isFieldAliveNextGeneration(int x, int y, Field currentGen) {
        return !hasToLessNeighbors(x,y,currentGen) && !hasToManyNeighbors(x,y,currentGen);
    }

    private boolean hasToLessNeighbors(int x, int y, Field currentGen) {
        return currentGen.getCountOfLivingNeighbors(x,y) <= LOWER_BORDER_OF_DIE;
    }

    private boolean hasToManyNeighbors(int x, int y, Field currentGen) {
        return currentGen.getCountOfLivingNeighbors(x,y) >= UPPER_BORDER_OF_DIE;
    }
}
