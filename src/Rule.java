public interface Rule {

    public boolean isFieldAliveNextGeneration(int x, int y, Field currentGen);
}
