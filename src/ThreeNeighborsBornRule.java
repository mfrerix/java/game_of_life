public class ThreeNeighborsBornRule implements Rule {

    private static final int NUMBER_OF_NEIGHBORS_FOR_BORN = 3;

    @Override
    public boolean isFieldAliveNextGeneration(int x, int y, Field currentGen) {
        return currentGen.getCountOfLivingNeighbors(x, y) == NUMBER_OF_NEIGHBORS_FOR_BORN;
    }
}
