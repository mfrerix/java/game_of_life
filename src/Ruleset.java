public interface Ruleset {

    public Field applyRulesToField(Field field);

}
